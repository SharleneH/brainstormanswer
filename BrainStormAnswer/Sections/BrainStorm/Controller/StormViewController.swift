//
//

import UIKit

class StormViewController: UIViewController {
    @IBOutlet var views: StormViews!
    private let viewModel: StormViewModel = StormViewModel()
    var mCountDown = 10
    var mTime:Timer?
    override func viewDidLoad() {
        super.viewDidLoad()
        views.resultLable.isHidden = true
        views.nextButton.isHidden = true
        views.scoreLabel.text = String(viewModel.score)
        viewModel.actionChoiceQueses = viewModel.choiceModeles
        setQues()
        self.mTime = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { (Timer) in
            self.countDownEvent()
        })

    }
    
    @IBAction func choose(_ sender: UIButton) {
        if let choice = sender.title(for: .normal), choice == viewModel.choiceModel.answer {
            viewModel.score += 10
            views.scoreLabel.text = String(viewModel.score)
            sender.setTitleColor(.green, for: .normal)
            views.choiceButton.forEach { btn in
                btn.isUserInteractionEnabled = false
            }
        } else {
            sender.setTitleColor(.red, for: .normal)
            views.choiceButton.forEach { btn in
                btn.isUserInteractionEnabled = false
                if btn.title(for: .normal) == viewModel.choiceModel.answer {
                    btn.setTitleColor(.green, for: .normal)
                }
            }
        }
        if viewModel.count >= 10 {
            final()
        } else {
            views.nextButton.isHidden = false
        }
    }

    @IBAction func next(_ sender: Any) {
        views.choiceButton.forEach { btn in
            btn.isUserInteractionEnabled = true
            btn.setTitleColor(.yellow, for: .normal)
        }
        setQues()
        views.nextButton.isHidden = true
        //倒數計時
        mCountDown = 10

    }

    @IBAction func Again(_ sender: Any) {
        views.resultLable.isHidden = true
        views.nextButton.isHidden = true
        views.scoreTitleLabel.isHidden = false
        views.countDownLabel.isHidden = false
        views.reStartButton.setTitle("重新來過", for: .normal)
        viewModel.count = 0
        viewModel.score = 0
        views.scoreLabel.text = String(viewModel.score)
        viewModel.actionChoiceQueses = viewModel.choiceModeles
        views.choiceButton.forEach { bt in
            bt.setTitleColor(UIColor.yellow, for: .normal)
            bt.isUserInteractionEnabled = true
            bt.isHidden = false
        }
        setQues()
    }

    //設定出現題目
    func setQues() {
        if viewModel.actionChoiceQueses.count != 0{
            viewModel.index = Int.random(in: 0..<viewModel.actionChoiceQueses.count)
            viewModel.choiceModel = viewModel.actionChoiceQueses.remove(at: viewModel.index)
            views.questionLable.text = viewModel.choiceModel.question
            views.choiceButton.forEach{ bt in
                bt.setTitleColor(.yellow , for: .normal)
                bt.isUserInteractionEnabled = true
            }
            for (i, choice) in viewModel.choiceModel.choice.shuffled().enumerated() {
                views.choiceButton[i].setTitle(choice, for: .normal)
            }
            //計算題數
            viewModel.count += 1
        }
    }

    //結果
    func final() {
        views.choiceButton.forEach { btn in
            btn.isUserInteractionEnabled = false
            btn.setTitle("", for: .disabled)
            btn.isHidden = true
        }
        views.questionLable.text = ""
        views.scoreLabel.text = ""
        views.nextButton.isHidden = true
        views.scoreTitleLabel.isHidden = true
        var msg = ""
        if viewModel.score > 80 {
            msg = "博學多聞"
        } else if viewModel.score > 60 {
            msg = "還不錯"
        } else if viewModel.score > 40 {
            msg = "再接再厲"
        } else if viewModel.score > 20 {
            msg = "井底之蛙"
        } else {
            msg = "哈囉～\n 你活在這個世界嗎？"
        }
        let resultMsg = "你得了\(viewModel.score)分\n\(msg)"
        views.resultLable.text = resultMsg
        views.resultLable.isHidden = false
        views.reStartButton.setTitle("再玩一次", for: .normal)
        self.mTime?.invalidate()
        self.views.countDownLabel.isHidden = true
    }
    
    func countDownEvent() {
        print("Hello I'm In!")
        if mCountDown != 0 {
            mCountDown -= 1
            views.countDownLabel.text = "倒數\n\(mCountDown)"
        }else {
            views.countDownLabel.text = "倒數\n0"
            mCountDown = 10
            setQues()
        }
    }
}
