//
//

import Foundation

struct StormModel {
    let question: String
    let choice: [String]
    let answer: String
    init(question: String, choice: [String], answer: String) {

        self.question = question
        self.choice = choice
        self.answer = answer
    }
}
